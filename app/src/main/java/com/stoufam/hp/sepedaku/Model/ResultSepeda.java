
package com.stoufam.hp.sepedaku.Model;

import java.util.HashMap;
import java.util.Map;
//import javax.annotation.Generated;

//@Generated("org.jsonschema2pojo")
public class ResultSepeda {

    private String nim;
    private String jenis;
    private String merk;
    private String type;
    private String warna;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 
     * @return
     *     The nim
     */
    public String getNim() {
        return nim;
    }

    /**
     * 
     * @param nim
     *     The nim
     */
    public void setNim(String nim) {
        this.nim = nim;
    }

    /**
     * 
     * @return
     *     The jenis
     */
    public String getJenis() {
        return jenis;
    }

    /**
     * 
     * @param jenis
     *     The jenis
     */
    public void setJenis(String jenis) {
        this.jenis = jenis;
    }

    /**
     * 
     * @return
     *     The merk
     */
    public String getMerk() {
        return merk;
    }

    /**
     * 
     * @param merk
     *     The merk
     */
    public void setMerk(String merk) {
        this.merk = merk;
    }

    /**
     * 
     * @return
     *     The type
     */
    public String getType() {
        return type;
    }

    /**
     * 
     * @param type
     *     The type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * 
     * @return
     *     The warna
     */
    public String getWarna() {
        return warna;
    }

    /**
     * 
     * @param warna
     *     The warna
     */
    public void setWarna(String warna) {
        this.warna = warna;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
