
package com.stoufam.hp.sepedaku.Model;

import java.util.HashMap;
import java.util.Map;
//import javax.annotation.Generated;

//@Generated("org.jsonschema2pojo")
public class ResultProfil {

    private String nim;
    private String nama;
    private String jurusan;
    private String fakultas;
    private String hp;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 
     * @return
     *     The nim
     */
    public String getNim() {
        return nim;
    }

    /**
     * 
     * @param nim
     *     The nim
     */
    public void setNim(String nim) {
        this.nim = nim;
    }

    /**
     * 
     * @return
     *     The nama
     */
    public String getNama() {
        return nama;
    }

    /**
     * 
     * @param nama
     *     The nama
     */
    public void setNama(String nama) {
        this.nama = nama;
    }

    /**
     * 
     * @return
     *     The jurusan
     */
    public String getJurusan() {
        return jurusan;
    }

    /**
     * 
     * @param jurusan
     *     The jurusan
     */
    public void setJurusan(String jurusan) {
        this.jurusan = jurusan;
    }

    /**
     * 
     * @return
     *     The fakultas
     */
    public String getFakultas() {
        return fakultas;
    }

    /**
     * 
     * @param fakultas
     *     The fakultas
     */
    public void setFakultas(String fakultas) {
        this.fakultas = fakultas;
    }

    /**
     * 
     * @return
     *     The hp
     */
    public String getHp() {
        return hp;
    }

    /**
     * 
     * @param hp
     *     The hp
     */
    public void setHp(String hp) {
        this.hp = hp;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
