
package com.stoufam.hp.sepedaku.Model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
//import javax.annotation.Generated;

//@Generated("org.jsonschema2pojo")
public class ModelSepeda {

    private List<ResultSepeda> resultSepeda = new ArrayList<ResultSepeda>();
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 
     * @return
     *     The resultSepeda
     */
    public List<ResultSepeda> getResultSepeda() {
        return resultSepeda;
    }

    /**
     * 
     * @param resultSepeda
     *     The resultSepeda
     */
    public void setResultSepeda(List<ResultSepeda> resultSepeda) {
        this.resultSepeda = resultSepeda;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
