
package com.stoufam.hp.sepedaku.Model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ModelProfil {

    private List<ResultProfil> resultProfil = null;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public List<ResultProfil> getResultProfil() {
        return resultProfil;
    }

    public void setResultProfil(List<ResultProfil> resultProfil) {
        this.resultProfil = resultProfil;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
