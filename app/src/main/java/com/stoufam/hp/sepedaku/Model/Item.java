
package com.stoufam.hp.sepedaku.Model;

//import javax.annotation.Generated;
//import com.google.gson.annotations.Expose;

import com.google.gson.annotations.SerializedName;

//@Generated("org.jsonschema2pojo")
public class Item {

    @SerializedName("nim")
    public String nim;
    @SerializedName("nama")
    private String nama;
    @SerializedName("jurusan")
    private String jurusan;
    @SerializedName("fakultas")
    private String fakultas;
    @SerializedName("hp")
    private String hp;
    @SerializedName("jenis")
    private String jenis;
    @SerializedName("merk")
    private String merk;
    @SerializedName("type")
    private String type;
    @SerializedName("warna")
    private String warna;

    /**
     * @return The nim
     */
    public String getNim() {
        return "NIm = "+nim;
    }

    /**
     * @param nim The nim
     */
    public void setNim(String nim) {
        this.nim = nim;
    }

    /**
     * @return The nama
     */
    public String getNama() {
        return nama;
    }

    /**
     * @param nama The nama
     */
    public void setNama(String nama) {
        this.nama = nama;
    }

    /**
     * @return The jurusan
     */
    public String getJurusan() {
        return jurusan;
    }

    /**
     * @param jurusan The jurusan
     */
    public void setJurusan(String jurusan) {
        this.jurusan = jurusan;
    }

    /**
     * @return The fakultas
     */
    public String getFakultas() {
        return fakultas;
    }

    /**
     * @param fakultas The fakultas
     */
    public void setFakultas(String fakultas) {
        this.fakultas = fakultas;
    }

    /**
     * @return The hp
     */
    public String getHp() {
        return hp;
    }

    /**
     * @param hp The hp
     */
    public void setHp(String hp) {
        this.hp = hp;
    }

    /**
     * @return The jenis
     */
    public String getJenis() {
        return jenis;
    }

    /**
     * @param jenis The jenis
     */
    public void setJenis(String jenis) {
        this.jenis = jenis;
    }

    /**
     * @return The merk
     */
    public String getMerk() {
        return merk;
    }

    /**
     * @param merk The merk
     */
    public void setMerk(String merk) {
        this.merk = merk;
    }

    /**
     * @return The type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type The type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return The warna
     */
    public String getWarna() {
        return warna;
    }

    /**
     * @param warna The warna
     */
    public void setWarna(String warna) {
        this.warna = warna;
    }

}
