package com.stoufam.hp.sepedaku.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.stoufam.hp.sepedaku.Model.Item;
import com.stoufam.hp.sepedaku.R;

import java.util.List;

/**
 * Created by hp on 11/3/2016.
 */

public class MainAdapter extends BaseAdapter {
    List<Item> stnk;
    Context context;
    LayoutInflater inflater;

    TextView mNim;
    TextView mNamaLengkap, mJurusan, mNoHandphone, mFakultas;
    TextView mJenis, mMerk, mType, mWarna;
    ImageView imFoto, imKtm;

    public MainAdapter(Context context, List<Item> stnk) {
        this.stnk = stnk;
        this.context = context;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return stnk.size();
    }

    @Override
    public Object getItem(int position) {

        return stnk.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //inisiasi


//        btSimpan = (Button) findViewById(R.id.button);
//        btFoto = (Button) findViewById(R.id.btEditPhotoProfil);
//        btKtm = (Button) findViewById(R.id.btEditPhotoKtm);
//
//        imFoto = (ImageView) findViewById(R.id.imageViewFoto);
//        imKtm = (ImageView) findViewById(R.id.imageViewKtm);


        if (inflater == null) {
            inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        }
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.item_stnk, null);
        }

        mNim = (TextView) convertView.findViewById(R.id.tvNim);
        mNamaLengkap = (TextView) convertView.findViewById(R.id.tvNama);
        mJurusan = (TextView) convertView.findViewById(R.id.tvJurusan);
        mFakultas = (TextView) convertView.findViewById(R.id.tvFakultas);
        mNoHandphone = (TextView) convertView.findViewById(R.id.tvHandphone);

        mJenis = (TextView) convertView.findViewById(R.id.tvJenis);
        mMerk = (TextView) convertView.findViewById(R.id.tvMerk);
        mType = (TextView) convertView.findViewById(R.id.tvType);
        mWarna = (TextView) convertView.findViewById(R.id.tvWarna);

        final String stringNim = stnk.get(position).getNim();
        mNim.setText(stringNim);

        mNamaLengkap.setText(stnk.get(position).getNama());
        mJurusan.setText(stnk.get(position).getJurusan());
        mFakultas.setText(stnk.get(position).getFakultas());
        mNoHandphone.setText(stnk.get(position).getHp());

        mJenis.setText(stnk.get(position).getJenis());
        mMerk.setText(stnk.get(position).getMerk());
        mType.setText(stnk.get(position).getType());
        mWarna.setText(stnk.get(position).getWarna());
        return convertView;
    }
}
