package com.stoufam.hp.sepedaku.Interface;


import com.stoufam.hp.sepedaku.Model.Item;
import com.stoufam.hp.sepedaku.Model.ModelProfil;
import com.stoufam.hp.sepedaku.Model.ModelSepeda;
import com.stoufam.hp.sepedaku.Model.ModelStnk;

import java.util.HashMap;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.QueryMap;

/**
 * Created by hp on 11/3/2016.
 */

public interface RestAPI {

    //create
    @FormUrlEncoded
    @POST("sepedaku/create.php")
    Call<ResponseBody> createUser (@FieldMap HashMap<String, String> params);

    @GET("sepedaku/get_profil.php")
    Call<ModelProfil> loadProfil(@QueryMap HashMap<String, String> params);

    //update profil
    @FormUrlEncoded
    @POST("sepedaku/update_profil.php")
    Call<ResponseBody> editProfil (@FieldMap HashMap<String, String> params);

    @GET("sepedaku/get_sepeda.php")
    Call<ModelSepeda> loadSepeda(@QueryMap HashMap<String, String> params);

    //update sepeda
    @FormUrlEncoded
    @POST("sepedaku/update_sepeda.php")
    Call<ResponseBody> editSepeda (@FieldMap HashMap<String, String> params);

    @GET("sepedaku/get_stnk.php")
    Call<ModelStnk> loadStnk(@QueryMap HashMap<String, String> params);
}
