package com.stoufam.hp.sepedaku;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.stoufam.hp.sepedaku.Adapter.MainAdapter;
import com.stoufam.hp.sepedaku.Interface.RestAPI;
import com.stoufam.hp.sepedaku.Model.Item;
import com.stoufam.hp.sepedaku.Model.ModelStnk;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class Stnk extends AppCompatActivity {

    TextView mNim;
    TextView mNamaLengkap, mJurusan, mNoHandphone, mFakultas;
    TextView mJenis, mMerk, mType, mWarna;
    Button btSimpan, btFoto, btKtm;
    ImageView imFoto, imKtm;

    RestAPI restAPI;
    View view;
    ScrollView view3;
    AdapterView view2;
    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stnk);
        restAPI = RestAPI.Factory.create();
        LoadData();

//inisiasi
        mNim = (TextView) findViewById(R.id.tvNim);
        mNamaLengkap = (TextView) findViewById(R.id.tvNama);
        mJurusan = (TextView) findViewById(R.id.tvJurusan);
        mFakultas = (TextView) findViewById(R.id.tvFakultas);
        mNoHandphone = (TextView) findViewById(R.id.tvHandphone);

        btSimpan = (Button) findViewById(R.id.button);
        btFoto = (Button) findViewById(R.id.btEditPhotoProfil);
        btKtm = (Button) findViewById(R.id.btEditPhotoKtm);

        imFoto = (ImageView) findViewById(R.id.imageViewFoto);
        imKtm = (ImageView) findViewById(R.id.imageViewKtm);

        mJenis = (TextView) findViewById(R.id.tvJenis);
        mMerk = (TextView) findViewById(R.id.tvMerk);
        mType = (TextView) findViewById(R.id.tvType);
        mWarna = (TextView) findViewById(R.id.tvWarna);


    }

    private void LoadData() {
        Call<ModelStnk> listData = restAPI.loadDataStnk();
        listData.enqueue(new Callback<ModelStnk>() {
            @Override
            public void onResponse(Call<ModelStnk> call, Response<ModelStnk> response) {
//                MainAdapter adapter = new MainAdapter(Stnk.this, response.body());
//                view.addView(adapter);
//                view2.setAdapter(adapter);
//                listView.setAdapter(adapter);

                mNim.setText(response.body().getItem().get(0).nim);
                mNamaLengkap.setText(response.body().getItem().get(0).getNama());
                mJurusan.setText(response.body().getItem().get(0).getJurusan());
            }

            @Override
            public void onFailure(Call<ModelStnk> call, Throwable t) {

            }
        });
    }
}
