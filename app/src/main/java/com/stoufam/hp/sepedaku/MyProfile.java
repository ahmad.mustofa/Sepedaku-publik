package com.stoufam.hp.sepedaku;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.stoufam.hp.sepedaku.Interface.RestAPI;
import com.stoufam.hp.sepedaku.Model.ModelProfil;
import com.stoufam.hp.sepedaku.Model.ResultProfil;

import java.util.HashMap;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.stoufam.hp.sepedaku.Config.ROOT_URL;


public class MyProfile extends AppCompatActivity {

    TextView mNim;
    EditText mNamaLengkap, mJurusan, mNoHandphone, mFakultas;
    Button btFoto, btKtm;
    ImageView imFoto, imKtm;
    private static final int CAMERA_REQUEST_CODE = 1;
    private static final int PICK_IMAGE_REQUEST = 2;

    public static String nim = "data";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_profile_2);

        //inisiasi
        mNim = (TextView) findViewById(R.id.etNim);
        mNamaLengkap = (EditText) findViewById(R.id.etNama);
        mJurusan = (EditText) findViewById(R.id.etJurusan);
        mFakultas = (EditText) findViewById(R.id.etFakultas);
        mNoHandphone = (EditText) findViewById(R.id.etHandphone);

        btFoto = (Button) findViewById(R.id.btEditPhotoProfil);
        btKtm = (Button) findViewById(R.id.btEditPhotoKtm);

        imFoto = (ImageView) findViewById(R.id.imageViewFoto);
        imKtm = (ImageView) findViewById(R.id.imageViewKtm);

        Intent intent = getIntent();
        nim = intent.getStringExtra(Config.EMP_ID);

        //Method panggil dan show data
        ShowData();

        btFoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent, 1);
            }
        });

        btKtm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent, 2);
            }
        });


    }

    private void ShowData() {

        //Ketika Aplikasi mengambil data kita akan melihat progress dialog
        final ProgressDialog loading = ProgressDialog.show(this, "Fetching Data", "Please Wait..", false, true);
        //Logging Interceptor
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        //set Level Log
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ROOT_URL)
                .addConverterFactory(GsonConverterFactory.create())//GsonConverter untuk parsing json
                .client(httpClient.build())
                .build();

        HashMap<String, String> params = new HashMap<>();
        params.put("id", nim);

        RestAPI service = retrofit.create(RestAPI.class);
        Call<ModelProfil> call = service.loadProfil(params);
        call.enqueue(new Callback<ModelProfil>() {  //Asyncronous Request
            @Override
            public void onResponse(Call<ModelProfil> call, Response<ModelProfil> response) {
                loading.dismiss();
                List<ResultProfil> buku = response.body().getResultProfil();

                //memasukkan data dari varibel buku ke jurusan
//                jurusan = buku;
                mNim.setText(nim);
                mNamaLengkap.setText(buku.get(0).getNama());
                mJurusan.setText(buku.get(0).getJurusan());
                mFakultas.setText(buku.get(0).getFakultas());
                mNoHandphone.setText(buku.get(0).getHp());

                //memanggil method untuk menampilkan list
//                showList();
            }

            @Override
            public void onFailure(Call<ModelProfil> call, Throwable t) {
                loading.dismiss();
                Toast.makeText(getApplicationContext(), "Gagal mengambil data", Toast.LENGTH_LONG).show();
            }
        });
    }

    void updateDataRetrofit() {
        final String nim = mNim.getText().toString().trim();
        final String nama = mNamaLengkap.getText().toString().trim();
        final String jurusan = mJurusan.getText().toString().trim();
        final String fakultas = mFakultas.getText().toString().trim();
        final String hp = mNoHandphone.getText().toString().trim();
//        String id_catatan = id;

        HashMap<String, String> params = new HashMap<>();
        params.put("nama", nama);
        params.put("jurusan", jurusan);
        params.put("fakultas", fakultas);
        params.put("hp", hp);
        params.put("nim", nim);

        //Ketika Aplikasi mengambil data kita akan melihat progress dialog
        final ProgressDialog loading = ProgressDialog.show(this, "Update Data", "Silahkan Tunggu...", false, false);
        loading.setCancelable(true);
        //Logging Interceptor
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        //set Level Log
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ROOT_URL)
                .addConverterFactory(GsonConverterFactory.create())//GsonConverter untuk parsing json
                .client(httpClient.build())
                .build();

        RestAPI api = retrofit.create(RestAPI.class);
        Call<ResponseBody> result = api.editProfil(params);
        result.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                loading.dismiss();
                try {
                    if (response.body() != null)
                        Toast.makeText(MyProfile.this, "Profil telah diupdate", Toast.LENGTH_LONG).show();
//                    Intent intent;
//                    intent = new Intent(EditCatatan.this, LihatCatatan.class);
//                    intent.putExtra(MainActivity.ID_CATATAN,id);
//                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                    startActivity(intent);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                loading.dismiss();
                Toast.makeText(MyProfile.this, "Profil gagal diupdate", Toast.LENGTH_LONG).show();
                t.printStackTrace();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_simpan, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_simpan) {
            updateDataRetrofit();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
//        if (btFoto.isSelected()) {
        if (requestCode == 1 && resultCode == Activity.RESULT_OK) {
            Bitmap bitmap;
            bitmap = (Bitmap) data.getExtras().get("data");
            imFoto.setImageBitmap(bitmap);
        } else if (requestCode == 2 && resultCode == Activity.RESULT_OK) {
            Bitmap bitmap;
            bitmap = (Bitmap) data.getExtras().get("data");
            imKtm.setImageBitmap(bitmap);

//            else if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
//                Uri filePath = data.getData();
//                try {
//                    //Getting the Bitmap from Gallery
//                    Bitmap bitmap;
//                    bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
//                    //Setting the Bitmap to ImageView
//                    imFoto.setImageBitmap(bitmap);
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//        }
//        if (btKtm.isSelected()) {
//            if (requestCode == CAMERA_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
//                Bitmap bitmap;
//                bitmap = (Bitmap) data.getExtras().get("data");
//                imKtm.setImageBitmap(bitmap);
//            } else if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
//                Uri filePath = data.getData();
//                try {
//                    //Getting the Bitmap from Gallery
//                    Bitmap bitmap;
//                    bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
//                    //Setting the Bitmap to ImageView
//                    imKtm.setImageBitmap(bitmap);
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//        }

        }
    }
}
