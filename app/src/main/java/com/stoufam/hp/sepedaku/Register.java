package com.stoufam.hp.sepedaku;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.stoufam.hp.sepedaku.Interface.RestAPI;

import java.util.HashMap;

import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.stoufam.hp.sepedaku.Config.ROOT_URL;

public class Register extends AppCompatActivity {

    //Mendefinisikan View Edit Text
    private EditText editTextId;
    private EditText editTextPassword;
    private EditText editTextNama;
    // Mendefinisikan View Button
    private Button buttonRegister;
    private Button buttonLihat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        //Men Inisialisasi View Text dan Button
        editTextId = (EditText) findViewById(R.id.nim_sign_up);
        editTextPassword = (EditText) findViewById(R.id.password_sign_uo);
        editTextNama = (EditText) findViewById(R.id.name_sign_up);

        buttonRegister = (Button) findViewById(R.id.buttonSignUp);

        buttonRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Tambah();
            }
        });
    }

    private void Tambah() {
        final String idMahasiswa = editTextId.getText().toString().trim();
        final String namaMahasiswa = editTextNama.getText().toString().trim();
        final String passMahasiswa = editTextPassword.getText().toString().trim();

        HashMap<String, String> params = new HashMap<>();
        params.put("nim", idMahasiswa);
        params.put("nama", namaMahasiswa);
        params.put("pass", passMahasiswa);

        //Ketika Aplikasi mengambil data kita akan melihat progress dialog
        final ProgressDialog loading = ProgressDialog.show(this, "Menyimpan Data", "Silahkan Tunggu...", false, true);
//        loading.setCancelable(true);
        //Logging Interceptor
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        //set Level Log
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ROOT_URL)
                .addConverterFactory(GsonConverterFactory.create())//GsonConverter untuk parsing json
                .client(httpClient.build())
                .build();

        RestAPI api = retrofit.create(RestAPI.class);
        Call<ResponseBody> result = api.createUser(params);
        result.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                loading.dismiss();
                try {
                    if (response.body() != null)
                        Toast.makeText(Register.this, "Anda Berhasil Mendaftar", Toast.LENGTH_LONG).show();
                    Intent intent;
                    intent = new Intent(Register.this, MainActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.putExtra(Config.EMP_ID, idMahasiswa);
                    startActivity(intent);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                loading.dismiss();
                Toast.makeText(Register.this, "Anda Gagal Mendaftar", Toast.LENGTH_LONG).show();
                t.printStackTrace();
            }
        });
    }
}
